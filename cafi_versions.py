#!/usr/bin/env python3
import requests
from pprint import pprint
from tabulate import tabulate

def fetch_version_json(url):
    return requests.get(url).json()

def build_version_dict(json, server_name):
    versions = []
    version_dict = {}
    for artifact in json['artifacts']:
        vstr = artifact['fileName'].strip('.zip')
        name, ver = vstr.split('-')[0], '-'.join(vstr.split('-')[1:])  # todo won't work for storage
        versions.append({'module' : name, 'version' : ver})

    branch = json['actions'][2]['lastBuiltRevision']['branch'][0]['name']
    version_dict.update({"versions" : versions})
    version_dict.update({"branch" : branch})
    version_dict.update({"server" : server_name})

    return version_dict

def get_hq_version_info():
    hq_latest_url = "http://10.90.100.40:8080/job/Build_cafi-suite_hq/lastSuccessfulBuild/api/json"
    return build_version_dict(fetch_version_json(hq_latest_url), "cafi-hq")

def get_dev_version_info():
    dev_latest_url = "http://10.90.100.40:8080/job/Build_cafi-suite_dev/lastSuccessfulBuild/api/json"
    return build_version_dict(fetch_version_json(dev_latest_url), "cafi-dev")

def get_prod_version_info():
    prod_latest_url = "http://10.90.100.40:8080/job/Build_cafi-suite_prod/lastSuccessfulBuild/api/json"
    return build_version_dict(fetch_version_json(prod_latest_url), "cafi-prod")

def main():
    hq = get_hq_version_info()
    dev = get_dev_version_info()
    prod = get_prod_version_info()

    print()
    for dct in [hq, dev, prod]:
        print(f"{dct['server']} --> {dct['branch']}")
        rows = []
        for v in dct['versions']:
            rows.append([v['module'], v['version']])
        print(tabulate(rows, headers=['Module', 'Version'], tablefmt='grid'))
        print()
        print()




if __name__ == "__main__":
    main()
